import React, { useEffect, useState } from 'react'
// @ts-ignore
import { createBrowserHistory } from 'history'
import { Route, Router, Switch } from 'react-router-dom'
import { ReactNotifications } from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'
import {
  Breadcrumb,
  Layout,
  Menu,
  Card,
  Col,
  Row,
  Button,
  Checkbox,
  Form,
  Input,
} from 'antd'

import { DesktopOutlined, PieChartOutlined } from '@ant-design/icons'
import 'antd/dist/antd.css'
import axios from 'axios'
import { Alert } from './components/Alert'

const { Header, Content, Footer, Sider } = Layout
const { Meta } = Card
export const history = createBrowserHistory()

function getItem(label: any, key: any, icon: any) {
  return {
    key,
    icon,
    label,
  }
}

const items = [
  getItem('List Item', '1', <PieChartOutlined />),
  getItem('Add Item', '2', <DesktopOutlined />),
]

const App = () => {
  const [collapsed, setCollapsed] = useState(false)
  const [data, setData] = useState([])
  const [activeTab, setActiveTab] = useState('1')

  const doPost = async (name: string, imageUrl: string) => {
    const headers = {
      Authorization: `Bearer keyxFjite9GHq6uXf`,
    }
    const data = {
      records: [
        {
          fields: {
            name,
            imageUrl,
          },
        },
      ],
    }
    let result = await axios.request({
      headers,
      url: 'https://api.airtable.com/v0/applEgJ9tXQgDknEH/Projects',
      data,
      method: 'POST',
    })
    return result.data
  }

  const onFinish = (values: any) => {
    doPost(values?.name, values?.url).then(r => {
      console.log('Success:', values)
      // alert('Add items success!')
      Alert.success('Add items success')
    })
  }

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo)
  }

  const onClick = (e: any) => {
    console.log('click ', e)
    setActiveTab(e?.key)
  }

  useEffect(() => {
    fetch(
      'https://api.airtable.com/v0/applEgJ9tXQgDknEH/Projects?api_key=keyxFjite9GHq6uXf',
    )
      .then(res => res.json())
      .then(json => {
        console.log('json____', json)
        setData(json?.records ?? [])
      })
  }, [activeTab])
  return (
    <>
      <ReactNotifications />
      <Layout
        style={{
          minHeight: '100vh',
        }}
      >
        <Sider
          collapsible
          collapsed={collapsed}
          onCollapse={value => setCollapsed(value)}
        >
          <div
            className="logo"
            style={{
              height: '32px',
              margin: '16px',
              background: 'rgba(255, 255, 255, 0.3)',
            }}
          />
          <Menu
            theme="dark"
            defaultSelectedKeys={[activeTab]}
            mode="inline"
            items={items}
            onClick={onClick}
          />
        </Sider>
        <Layout className="site-layout">
          <Header
            className="site-layout-background"
            style={{
              padding: 0,
            }}
          />
          <Content
            style={{
              margin: '0 16px',
            }}
          >
            <Breadcrumb
              style={{
                margin: '16px 0',
              }}
            >
              <Breadcrumb.Item>List item</Breadcrumb.Item>
            </Breadcrumb>
            <div
              className="site-layout-background"
              style={{
                padding: 24,
                minHeight: 360,
              }}
            >
              {activeTab === '1' && (
                <div className="site-card-wrapper">
                  <Row gutter={16}>
                    {data.length > 0 &&
                      data?.map((item: any) => (
                        <Col key={item?.id}>
                          <Card
                            hoverable
                            style={{
                              width: 240,
                            }}
                            cover={
                              <img
                                style={{
                                  width: 240,
                                  height: 240,
                                }}
                                alt="example"
                                src={item?.fields?.imageUrl}
                              />
                            }
                          >
                            <Meta title={item?.fields?.name} />
                          </Card>
                        </Col>
                      ))}
                  </Row>
                </div>
              )}

              {activeTab === '2' && (
                <Form
                  name="basic"
                  labelCol={{
                    span: 8,
                  }}
                  wrapperCol={{
                    span: 8,
                  }}
                  initialValues={{
                    remember: true,
                  }}
                  onFinish={onFinish}
                  onFinishFailed={onFinishFailed}
                  autoComplete="off"
                >
                  <Form.Item
                    label="Name"
                    name="name"
                    rules={[
                      {
                        required: true,
                        message: 'Please input name!',
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    label="Url Image"
                    name="url"
                    rules={[
                      {
                        required: true,
                        message: 'Please input url!',
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    wrapperCol={{
                      offset: 8,
                      span: 8,
                    }}
                  >
                    <Button type="primary" htmlType="submit">
                      Submit
                    </Button>
                  </Form.Item>
                </Form>
              )}
            </div>
          </Content>
          <Footer
            style={{
              textAlign: 'center',
            }}
          >
            Michale An
          </Footer>
        </Layout>
      </Layout>
    </>
  )
}

export default App
