import { Store } from 'react-notifications-component'

const defaultOption = {
  insert: 'top',
  container: 'bottom-right',
  animationIn: ['animate__animated', 'animate__fadeIn'],
  animationOut: ['animate__animated', 'animate__fadeOut'],
  width: 250,
  dismiss: {
    duration: 3500,
    onScreen: true,
    showIcon: true,
    click: true,
  },
}

function success(message: string) {
  const options: any = {
    ...defaultOption,
    message,
    type: 'success',
  }
  Store.addNotification(options)
}

function warning(message: string) {
  const options: any = {
    ...defaultOption,
    message,
    type: 'warning',
  }
  Store.addNotification(options)
}

function info(message: string) {
  const options: any = {
    ...defaultOption,
    message,
    type: 'info',
  }
  Store.addNotification(options)
}

function error(message: string) {
  const options: any = {
    ...defaultOption,
    message,
    type: 'danger',
  }
  Store.addNotification(options)
}

export const Alert = {
  success,
  info,
  error,
  warning,
}
